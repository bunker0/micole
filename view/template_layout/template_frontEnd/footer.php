  <!--Footer-->
  <footer id="footer" class="footer">
    <div class="container text-center">
    
      <ul class="social-links">
        <li><a href="#link"><i class="fab fa-twitter"></i></a></li>
        <li><a href="#link"><i class="fab fa-facebook-square"></i></a></li>
        <li><a href="#link"><i class="fab fa-google-plus-g"></i></a></li>
      </ul>

      <div class="credits">

        ©2020 MiCole.net Todos los derechos reservados
      </div>
    </div>
  </footer>
  <!--/ Footer-->
  <?php include_once'view\template_layout\template_frontEnd\script.php' ?>